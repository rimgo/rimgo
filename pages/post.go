package pages

import (
	"crypto/rand"
	"fmt"
	"strconv"
	"strings"

	"codeberg.org/rimgo/rimgo/api"
	"codeberg.org/rimgo/rimgo/utils"
	"github.com/gofiber/fiber/v2"
)

// Cursed function
func nextInTag(client *api.Client, tagname, sort, page, I string) string {
	i, err := strconv.Atoi(I)
	if err != nil || i < 0 {
		return ""
	}
	tag, err := client.FetchTag(tagname, sort, page)
	if err != nil {
		return ""
	}
	if i >= len(tag.Posts)-1 {
		pageNumber, _ := strconv.Atoi(page)
		tagn, err := client.FetchTag(tagname, sort, strconv.Itoa(pageNumber+1))
		// Check length - Imgur will not return an error if there are no more posts and you request the next page
		if err != nil || len(tagn.Posts) < 1 {
			return ""
		}
		return tagn.Posts[0].Link
	}
	return tag.Posts[i+1].Link
}

func HandlePost(c *fiber.Ctx) error {
	utils.SetHeaders(c)
	c.Set("X-Frame-Options", "DENY")

	postId := c.Params("postID")
	if strings.Contains(postId, "-") {
		postId = postId[len(postId)-7:]
	}

	post, err := api.Album{}, error(nil)
	switch {
	case strings.HasPrefix(c.Path(), "/a"):
		post, err = ApiClient.FetchAlbum(postId)
	case strings.HasPrefix(c.Path(), "/gallery"):
		post, err = ApiClient.FetchPosts(postId)
	case strings.HasPrefix(c.Path(), "/t"):
		post, err = ApiClient.FetchPosts(postId)
	default:
		post, err = ApiClient.FetchMedia(postId)
	}
	if err != nil && err.Error() == "ratelimited by imgur" {
		return utils.RenderError(c, 429)
	}
	if err != nil && post.Id == "" && strings.Contains(err.Error(), "404") {
		return utils.RenderError(c, 404)
	}
	if err != nil {
		return err
	}

	comments := []api.Comment{}
	if post.SharedWithCommunity {
		c.Set("Cache-Control", "public,max-age=604800")
		comments, err = ApiClient.FetchComments(postId)
		if err != nil {
			return err
		}
	} else {
		c.Set("Cache-Control", "public,max-age=31557600")
	}

	nonce := ""
	csp := "default-src 'none'; frame-ancestors 'none'; base-uri 'none'; form-action 'self'; media-src 'self'; img-src 'self'; manifest-src 'self'; block-all-mixed-content; style-src 'self'"
	if len(post.Tags) != 0 {
		b := make([]byte, 8)
		rand.Read(b)
		nonce = fmt.Sprintf("%x", b)
		csp = csp + " 'nonce-" + nonce + "'"
	}
	c.Set("Content-Security-Policy", csp)

	var next string
	tagParam := strings.Split(c.Query("tag"), ".")
	if len(tagParam) == 4 {
		tag, sort, page, index := tagParam[0], tagParam[1], tagParam[2], tagParam[3]
		next = nextInTag(ApiClient, tag, sort, page, index)
	}

	return c.Render("post", fiber.Map{
		"post":     post,
		"next":     next,
		"comments": comments,
		"nonce":    nonce,
	})
}
