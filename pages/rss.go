package pages

import (
	"time"

	"codeberg.org/rimgo/rimgo/api"
	"codeberg.org/rimgo/rimgo/utils"
	"github.com/gofiber/fiber/v2"
	"github.com/gorilla/feeds"
)

func HandleTagRSS(c *fiber.Ctx) error {
	utils.SetHeaders(c)

	tag, err := ApiClient.FetchTag(c.Params("tag"), c.Query("sort"), "1")
	if err != nil && err.Error() == "ratelimited by imgur" {
		return c.Status(429).SendString("rate limited by imgur")
	}
	if err != nil {
		return err
	}
	if tag.Display == "" {
		return c.Status(404).SendString("tag not found")
	}

	instance := utils.GetInstanceUrl(c)

	feed := &feeds.Feed{
		Title:   tag.Display + " on Imgur",
		Link:    &feeds.Link{Href: instance + "/t/" + c.Params("tag")},
		Created: time.Now(),
	}

	return handleFeed(c, instance, feed, tag.Posts)
}

func HandleTrendingRSS(c *fiber.Ctx) error {
	utils.SetHeaders(c)

	section := c.Query("section")
	switch section {
	case "hot", "new", "top":
	default:
		section = "hot"
	}
	sort := c.Query("sort")
	switch sort {
	case "newest", "best", "popular":
	default:
		sort = "popular"
	}

	results, err := ApiClient.FetchTrending(section, sort, "1")
	if err != nil {
		return err
	}

	instance := utils.GetInstanceUrl(c)

	feed := &feeds.Feed{
		Title:   "Trending on Imgur",
		Link:    &feeds.Link{Href: instance + "/trending"},
		Created: time.Now(),
	}

	return handleFeed(c, instance, feed, results)
}

func HandleUserRSS(c *fiber.Ctx) error {
	utils.SetHeaders(c)

	user := c.Params("userID")

	submissions, err := ApiClient.FetchSubmissions(user, "newest", "1")
	if err != nil && err.Error() == "ratelimited by imgur" {
		c.Status(429)
		return utils.RenderError(c, 429)
	}
	if err != nil {
		return err
	}

	instance := utils.GetInstanceUrl(c)

	feed := &feeds.Feed{
		Title:   user + " on Imgur",
		Link:    &feeds.Link{Href: instance + "/user/" + user},
		Created: time.Now(),
	}

	return handleFeed(c, instance, feed, submissions)
}

func handleFeed(c *fiber.Ctx, instance string, feed *feeds.Feed, posts []api.Submission) error {
	feed.Items = []*feeds.Item{}

	for _, post := range posts {
		link := instance + post.Link

		item := &feeds.Item{
			Title:       post.Title,
			Link:        &feeds.Link{Href: link},
			Description: "<a href=\"" + link + "\"><img width=\"480\" src=\"" + instance + "/" + post.Cover.Id + ".jpeg" + "\"></a>",
		}

		if post.Cover.Type == "video" {
			item.Description = "🎞️ Video<br><br>" + item.Description
		}

		feed.Items = append(feed.Items, item)
	}

	c.Type(c.Params("type"))
	switch c.Params("type") {
	case "atom":
		body, err := feed.ToAtom()
		if err != nil {
			return err
		}
		return c.SendString(body)
	case "json":
		body, err := feed.ToJSON()
		if err != nil {
			return err
		}
		return c.JSON(body)
	case "rss":
		body, err := feed.ToRss()
		if err != nil {
			return err
		}
		return c.SendString(body)
	default:
		return c.Status(400).SendString("invalid type")
	}
}
